(function () {
    'use strict';
    angular
        .module('App')
    .controller('Users.Controller', UsersController);
    
    function UsersController($scope, $http, $log) {

        $http.get('http://localhost:2021/api/Users')
        .success(function (result) {
            $scope.users = result;
            $log.info(result);
        })
        .error(function (data, status) {
            $log.error(data);
        });
    }
    
})();