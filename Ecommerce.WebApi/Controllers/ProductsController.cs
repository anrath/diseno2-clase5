﻿using Ecommerce.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;


namespace Ecommerce.WebApi.Controllers
{
    public class ProductsController : ApiController
    {

        public IEnumerable<Product> GetAllProducts()
        {
            using (var ctx = new Ecommerce.DataAccess.EcommerceContext())
            {
                return ctx.Products.Include("Features").Include("Images").Include("Videos").ToArray();
            }
        }

        public IHttpActionResult GetProduct(int id)
        {

            using (var ctx = new Ecommerce.DataAccess.EcommerceContext())
            {
                var product = ctx.Products.Include("Features").Include("Images").Include("Videos").FirstOrDefault((u) => u.ProductID == id);
                if (product == null)
                {
                    return NotFound();
                }
                return Ok(product);
            }

        }
    }
}