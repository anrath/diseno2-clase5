﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Entities
{
    public class Video
    {

        public int VideoID { get; set; }
        public string VideoLink { get; set; }
        public Video()
        {

        }

    }
}
